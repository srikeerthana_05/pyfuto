import random

def generate_puzzle_and_solution(size, difficulty):
    filled_grid = generate_filled_grid(size)
    num_holes = int(size**2 * difficulty)
    num_inequalities = max(size + difficulty * size, 5)
    inequalities = generate_inequalities(filled_grid, num_inequalities)
    puzzle = create_puzzle(filled_grid, num_holes)

    solution = [row[:] for row in puzzle]
    if solve_futoshiki(solution, inequalities):
        return puzzle, solution, inequalities
    else:
        return generate_puzzle_and_solution(size, difficulty)

def generate_filled_grid(size):
    grid = [[0 for _ in range(size)] for _ in range(size)]

    def fill_grid(grid, row, col):
        if row == size:
            return True
        if col == size:
            return fill_grid(grid, row + 1, 0)

        nums = list(range(1, size + 1))
        random.shuffle(nums)
        for num in nums:
            if is_valid(grid, row, col, num, []):
                grid[row][col] = num
                if fill_grid(grid, row, col + 1):
                    return True
                grid[row][col] = 0

        return False

    fill_grid(grid, 0, 0)
    return grid

def create_puzzle(grid, num_holes):
    puzzle = [row[:] for row in grid]
    size = len(grid)
    removed_positions = set()

    while len(removed_positions) < num_holes:
        row = random.randint(0, size - 1)
        col = random.randint(0, size - 1)
        if (row, col) not in removed_positions:
            puzzle[row][col] = 0
            removed_positions.add((row, col))

    return puzzle

def generate_inequalities(grid, num_inequalities):
    inequalities = []
    size = len(grid)
    inequality_positions = set()

    while len(inequalities) < num_inequalities:
        row1, col1 = random.randint(0, size - 1), random.randint(0, size - 1)
        direction = random.choice(['row', 'col'])
        if direction == 'row' and col1 < size - 1:
            row2, col2 = row1, col1 + 1
        elif direction == 'col' and row1 < size - 1:
            row2, col2 = row1 + 1, col1
        else:
            continue

        if (row1, col1, row2, col2) in inequality_positions or (row2, col2, row1, col1) in inequality_positions:
            continue

        if grid[row1][col1] < grid[row2][col2]:
            inequalities.append((row1, col1, row2, col2, '<'))
            inequality_positions.add((row1, col1, row2, col2))
        elif grid[row1][col1] > grid[row2][col2]:
            inequalities.append((row1, col1, row2, col2, '>'))
            inequality_positions.add((row1, col1, row2, col2))

    return inequalities

def solve_futoshiki(grid, inequalities):
    size = len(grid)
    for row in range(size):
        for col in range(size):
            if grid[row][col] == 0:
                for num in range(1, size + 1):
                    if is_valid(grid, row, col, num, inequalities):
                        grid[row][col] = num
                        if solve_futoshiki(grid, inequalities):
                            return True
                        grid[row][col] = 0
                return False
    return True

def is_valid(grid, row, col, num, inequalities):
    size = len(grid)
    for i in range(size):
        if grid[row][i] == num or grid[i][col] == num:
            return False

    for (r1, c1, r2, c2, relation) in inequalities:
        if r1 == row and c1 == col:
            if relation == '<' and num >= grid[r2][c2] and grid[r2][c2] != 0:
                return False
            if relation == '>' and num <= grid[r2][c2] and grid[r2][c2] != 0:
                return False
        elif r2 == row and c2 == col:
            if relation == '<' and num <= grid[r1][c1] and grid[r1][c1] != 0:
                return False
            if relation == '>' and num >= grid[r1][c1] and grid[r1][c1] != 0:
                return False

    return True

