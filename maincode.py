import cv2
import pytesseract
import numpy as np
import pygame
import sys

# Image Processing Functions
def preprocess_image(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    if image is None:
        print("Error: Unable to load image. Check the image path.")
        sys.exit()
    _, binary_image = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    return binary_image

def extract_text_from_image(image):
    config = "--psm 6"
    text = pytesseract.image_to_string(image, config=config)
    print(f"Extracted text: {text}")
    return text

def parse_puzzle(text):
    lines = text.split('\n')
    grid = []
    constraints = []
    for line in lines:
        if line.strip():
            row = []
            for char in line.strip():
                if char.isdigit():
                    row.append(int(char))
                elif char in ['<', '>']:
                    constraints.append((row[-1], char))
                else:
                    row.append(0)
            grid.append(row)
    print(f"Parsed grid: {grid}")
    print(f"Parsed constraints: {constraints}")
    return grid, constraints

# Futoshiki Solver Class
class FutoshikiSolver:
    def __init__(self, grid, constraints):
        self.grid = grid
        self.constraints = constraints
        self.size = len(grid)

    def is_valid(self, row, col, num):
        for i in range(self.size):
            if self.grid[row][i] == num or self.grid[i][col] == num:
                return False
        for constraint in self.constraints:
            (r1, c1), (r2, c2), relation = constraint
            if (r1 == row and c1 == col) or (r2 == row and c2 == col):
                if relation == '>' and not (self.grid[r1][c1] > self.grid[r2][c2]):
                    return False
                if relation == '<' and not (self.grid[r1][c1] < self.grid[r2][c2]):
                    return False
        return True

    def solve(self):
        for row in range(self.size):
            for col in range(self.size):
                if self.grid[row][col] == 0:
                    for num in range(1, self.size + 1):
                        if self.is_valid(row, col, num):
                            self.grid[row][col] = num
                            if self.solve():
                                return True
                            self.grid[row][col] = 0
                    return False
        return True

# Pygame Interactive Gameplay
def draw_grid():
    for row in range(GRID_SIZE):
        for col in range(GRID_SIZE):
            rect = pygame.Rect(col * CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE)
            pygame.draw.rect(screen, BLACK, rect, 1)

def draw_numbers(grid):
    font = pygame.font.Font(None, 36)
    for row in range(GRID_SIZE):
        for col in range(GRID_SIZE):
            if grid[row][col] != 0:
                text = font.render(str(grid[row][col]), True, BLACK)
                screen.blit(text, (col * CELL_SIZE + 15, row * CELL_SIZE + 10))

def main():
    # Image Processing
    image_path = 'input_puzzle.png'
    preprocessed_image = preprocess_image(image_path)
    extracted_text = extract_text_from_image(preprocessed_image)
    puzzle_grid, puzzle_constraints = parse_puzzle(extracted_text)
    
    # Solve Puzzle if requested
    solver = FutoshikiSolver(puzzle_grid, puzzle_constraints)
    if solver.solve():
        solved_grid = solver.grid
        print("Solved grid:")
        for row in solved_grid:
            print(row)
    else:
        print("Failed to solve the puzzle.")
        solved_grid = puzzle_grid

    # Initialize Pygame
    pygame.init()
    global screen, GRID_SIZE, CELL_SIZE, WHITE, BLACK
    WINDOW_SIZE = 500
    GRID_SIZE = 5
    CELL_SIZE = WINDOW_SIZE // GRID_SIZE
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    screen = pygame.display.set_mode((WINDOW_SIZE, WINDOW_SIZE))
    pygame.display.set_caption('Futoshiki Solver')

    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    running = False

        screen.fill(WHITE)
        draw_grid()
        draw_numbers(solved_grid)
        pygame.display.flip()

    pygame.quit()
    sys.exit()

if __name__ == '__main__':
    main()



