import random
import pygame
import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
from pygame import mixer



def add_background(root, image_path):
    
    image = Image.open(image_path)
    window_width = root.winfo_screenwidth()
    window_height = root.winfo_screenheight()
    image = image.resize((window_width, window_height))
    bg_image = ImageTk.PhotoImage(image)
    bg_label = tk.Label(root, image=bg_image)
    bg_label.image = bg_image  
    bg_label.place(relwidth=1, relheight=1)

def add_background_music():
    
    pygame.mixer.init()
    mixer.music.load(r"back_music.wav")
    mixer.music.play(-1) 

def exit_application(window):
    
    window.destroy()

def show_solution():
    
    global solution_displayed
    solution_displayed = True
    messagebox.showinfo("Solution", "The solution is now displayed.")

def submit_solution():

    if player_grid == solution:
        messagebox.showinfo("Result", "Congratulations! You solved the puzzle!")
    else:
        messagebox.showinfo("Result", "Sorry, your solution is incorrect. The correct solution is now displayed.")
    show_solution()

def draw_puzzle(screen, grid, inequalities, cell_size=100, gap_size=38, margin_size=20, show_solution=False, player_grid=None, selected_cell=None):

    font = pygame.font.Font(None, 72)
    small_font = pygame.font.Font(None, 48)  
    screen.fill((220, 220, 220))  

    for row in range(4):
        for col in range(4):
            x = col * (cell_size + gap_size) + margin_size
            y = row * (cell_size + gap_size) + margin_size
            rect = pygame.Rect(x, y, cell_size, cell_size)
            if (row, col) == selected_cell:
                pygame.draw.rect(screen, (255, 255, 0), rect)  
            else:
                pygame.draw.rect(screen, (255, 255, 255), rect)
            pygame.draw.rect(screen, (0, 0, 0), rect, 2)  

            if player_grid is not None and player_grid[row][col] != 0:
                num = player_grid[row][col]
                text = font.render(str(num), True, (0, 128, 0))  
            else:
                num = grid[row][col] if not show_solution else solution[row][col]
                if num != 0:
                    text = font.render(str(num), True, (0, 0, 0))
            if num != 0:
                text_rect = text.get_rect(center=(x + cell_size / 2, y + cell_size / 2))
                screen.blit(text, text_rect)

    for (r1, c1, r2, c2, relation) in inequalities:
        if r1 == r2:  
            x = min(c1, c2) * (cell_size + gap_size) + cell_size + (gap_size // 2) + margin_size
            y = r1 * (cell_size + gap_size) + cell_size / 2 + margin_size
            if relation == '>':
                arrow = small_font.render('>', True, (0, 0, 0))
            else:
                arrow = small_font.render('<', True, (0, 0, 0))
            screen.blit(arrow, (x - arrow.get_width() / 2, y - arrow.get_height() / 2))
        elif c1 == c2: 
            x = c1 * (cell_size + gap_size) + cell_size / 2 + margin_size
            y = min(r1, r2) * (cell_size + gap_size) + cell_size + (gap_size // 2) + margin_size
            if relation == '>':
                arrow = small_font.render('v', True, (0, 0, 0))  
            else:
                arrow = small_font.render('^', True, (0, 0, 0))  
            screen.blit(arrow, (x - arrow.get_width() / 2, y - arrow.get_height() / 2))

def print_grid(grid):

    for row in grid:
        print(" ".join(str(val) if val != 0 else "." for val in row))
    print()

def is_valid(grid, row, col, num, inequalities):

    if num in grid[row] or num in [grid[i][col] for i in range(4)]:
        return False

    for (r1, c1, r2, c2, relation) in inequalities:
        if r1 == row and c1 == col:
            if relation == '<' and not (grid[r2][c2] == 0 or num < grid[r2][c2]):
                return False
            if relation == '>' and not (grid[r2][c2] == 0 or num > grid[r2][c2]):
                return False
        elif r2 == row and c2 == col:
            if relation == '<' and not (grid[r1][c1] == 0 or grid[r1][c1] < num):
                return False
            if relation == '>' and not (grid[r1][c1] == 0 or grid[r1][c1] > num):
                return False

    return True

def solve_futoshiki(grid, inequalities):

    for row in range(4):
        for col in range(4):
            if grid[row][col] == 0:
                for num in range(1, 5):
                    if is_valid(grid, row, col, num, inequalities):
                        grid[row][col] = num
                        if solve_futoshiki(grid, inequalities):
                            return True
                        grid[row][col] = 0

                return False
    return True

def generate_filled_grid():

    nums = list(range(1, 5))
    grid = [[0] * 4 for _ in range(4)]

    def fill_grid(r, c):
        
        if r == 4:
            return True
        next_r, next_c = (r, c + 1) if c < 3 else (r + 1, 0)
        random.shuffle(nums)
        for num in nums:
            if is_valid(grid, r, c, num, []):
                grid[r][c] = num
                if fill_grid(next_r, next_c):
                    return True
                grid[r][c] = 0
        return False

    fill_grid(0, 0)
    return grid

def create_puzzle(grid, num_holes=6):

    puzzle = [row[:] for row in grid]
    removed_positions = set()

    while len(removed_positions) < num_holes:
        row = random.randint(0, 3)
        col = random.randint(0, 3)
        if (row, col) not in removed_positions:
            puzzle[row][col] = 0
            removed_positions.add((row, col))

    return puzzle

def generate_inequalities(grid):

    inequalities = []
    inequality_positions = set()

    while len(inequalities) < 5:
        row1, col1 = random.randint(0, 3), random.randint(0, 3)
        direction = random.choice(['row', 'col'])
        if direction == 'row' and col1 < 3:
            row2, col2 = row1, col1 + 1
        elif direction == 'col' and row1 < 3:
            row2, col2 = row1 + 1, col1
        else:
            continue

        if (row1, col1, row2, col2) in inequality_positions or (row2, col2, row1, col1) in inequality_positions:
            continue

        if grid[row1][col1] < grid[row2][col2]:
            inequalities.append((row1, col1, row2, col2, '<'))
            inequality_positions.add((row1, col1, row2, col2))
        elif grid[row1][col1] > grid[row2][col2]:
            inequalities.append((row1, col1, row2, col2, '>'))
            inequality_positions.add((row1, col1, row2, col2))

    return inequalities


def level_selection_window():

    global level_root
    level_root = tk.Tk()
    level_root.title("Select Difficulty Level")

    level_root.attributes("-fullscreen", True)

    add_background(level_root, r"game_bg.jpg")

    easy_button = tk.Button(level_root, height=1, width=15, text="Easy", font=("COPPERPLATE GOTHIC BOLD", 25), command=lambda: main_game_window('Easy'))
    easy_button.place(relx=0.5, rely=0.4, anchor=tk.CENTER)

    medium_button = tk.Button(level_root, height=1, width=15, text="Medium", font=("COPPERPLATE GOTHIC BOLD", 25), command=lambda: main_game_window('Medium'))
    medium_button.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

    hard_button = tk.Button(level_root, height=1, width=15, text="Hard", font=("COPPERPLATE GOTHIC BOLD", 25), command=lambda: main_game_window('Hard'))
    hard_button.place(relx=0.5, rely=0.6, anchor=tk.CENTER)

    back_home_button = tk.Button(level_root, text="Back to Home", font=("COPPERPLATE GOTHIC BOLD", 25), command=lambda: [level_root.destroy(), home_window()])
    back_home_button.place(relx=0.5, rely=0.7, anchor=tk.CENTER)

    exit_button = tk.Button(level_root, text="Exit", font=("COPPERPLATE GOTHIC BOLD", 25), command=lambda: exit_application(level_root))
    exit_button.place(relx=0.5, rely=0.8, anchor=tk.CENTER)

    level_root.mainloop()
def main_game_window(level):

    global root, screen, solution_displayed, player_grid, selected_cell, solution, puzzle, inequalities
    pygame.init()
    level_root.destroy()
    root = tk.Tk()
    root.title(f"Futoshiki Puzzle - {level} Level")

    add_background(root, r"game_bg.jpg")

    puzzle, solution, inequalities = generate_puzzle_and_solution(level)
    player_grid = [row[:] for row in puzzle] 
    print(f"Generated Futoshiki Puzzle ({level} Level):")
    print_grid(puzzle)
    print("Solution:")
    print_grid(solution)
    print("Inequalities:")
    for ineq in inequalities:
        print(f"Cell ({ineq[0] + 1},{ineq[1] + 1}) {ineq[4]} Cell ({ineq[2] + 1},{ineq[3] + 1})")

    cell_size = 100  
    gap_size = 38   
    margin_size = 20 
    screen_width = 4 * (cell_size + gap_size) - gap_size + 2 * margin_size
    screen_height = 4 * (cell_size + gap_size) - gap_size + 2 * margin_size
    screen = pygame.Surface((screen_width, screen_height))

    solution_displayed = False
    selected_cell = None
    draw_puzzle(screen, puzzle, inequalities, cell_size=cell_size, gap_size=gap_size, margin_size=margin_size, selected_cell=selected_cell)

    canvas = tk.Canvas(root, width=screen_width, height=screen_height)
    canvas.place(x=500, y=100)

    def update_display():
        
        draw_puzzle(screen, puzzle, inequalities, cell_size=cell_size, gap_size=gap_size, margin_size=margin_size, show_solution=solution_displayed, player_grid=player_grid, selected_cell=selected_cell)

        image_string = pygame.image.tostring(screen, "RGB")
        image_surface = Image.frombytes("RGB", screen.get_size(), image_string)
        photo = ImageTk.PhotoImage(image_surface)

        canvas.create_image(0, 0, anchor=tk.NW, image=photo)
        canvas.image = photo 

        root.after(100, update_display)

    solution_button = tk.Button(root, text="Show Solution", height=1, width=15, font=("COPPERPLATE GOTHIC BOLD", 22), command=show_solution)
    solution_button.place(x=1200, y=200)

    submit_button = tk.Button(root, text="Submit Solution", height=1, width=15, font=("COPPERPLATE GOTHIC BOLD", 22), command=submit_solution)
    submit_button.place(x=1200, y=300)

    exit_button = tk.Button(root, text="Exit", height=1, width=15, font=("COPPERPLATE GOTHIC BOLD", 22), command=lambda: exit_application(root))
    exit_button.place(x=1200, y=700)

    back_button = tk.Button(root, text="Back to Levels", height=1, width=15, font=("COPPERPLATE GOTHIC BOLD", 22), command=lambda: [root.destroy(), level_selection_window()])
    back_button.place(x=1200, y=400)

    def handle_key_event(event):

        if event.keysym.isdigit() and 1 <= int(event.keysym) <= 4:
            num = int(event.keysym)
            if selected_cell:
                row, col = selected_cell
                if puzzle[row][col] == 0:  
                    player_grid[row][col] = num
                    update_display()

    def handle_mouse_click(event):

        global selected_cell
        x, y = event.x, event.y
        col = (x - margin_size) // (cell_size + gap_size)
        row = (y - margin_size) // (cell_size + gap_size)
        if 0 <= row < 4 and 0 <= col < 4:
            selected_cell = (row, col)
            update_display()

    
    canvas.bind("<Button-1>", handle_mouse_click)
    root.bind("<Key>", handle_key_event)

    update_display()
    root.attributes("-fullscreen", True)
    add_background_music()
    root.mainloop()
    pygame.quit()


def generate_puzzle_and_solution(level):

    filled_grid = generate_filled_grid()
    num_holes = 6 if level == 'Easy' else 8 if level == 'Medium' else 10
    puzzle = create_puzzle(filled_grid, num_holes=num_holes)
    inequalities = generate_inequalities(filled_grid)

    solution_grid = [row[:] for row in filled_grid]  
    return puzzle, solution_grid, inequalities

def show_instructions():

    instructions_window = tk.Toplevel(home_root)
    instructions_window.title("Instructions")

    instructions_window.attributes("-fullscreen", True)
    instructions_window.bind("<Escape>", lambda e: instructions_window.destroy())
    screen_width = instructions_window.winfo_screenwidth()
    screen_height = instructions_window.winfo_screenheight()
    
    image_path = r"Rules for futoshiki..jpg"  
    image = Image.open(image_path)
    image = image.resize((screen_width, screen_height), Image.LANCZOS)
    
    bg_image = ImageTk.PhotoImage(image)
    
    bg_label = tk.Label(instructions_window, image=bg_image)
    bg_label.image = bg_image 
    bg_label.pack(fill=tk.BOTH, expand=True)

    
    instructions_window.geometry(f"{screen_width}x{screen_height}")

    
    back_button = tk.Button(instructions_window, text="Back to Home", font=("COPPERPLATE GOTHIC BOLD", 25), command=lambda: [instructions_window.destroy(), home_root.deiconify()])
    back_button.pack(pady=20) 

    
    back_button.place(relx=0.5, rely=0.9, anchor=tk.CENTER)  


def home_window():

    global home_root
    home_root = tk.Tk()
    home_root.title("Home")

    home_root.attributes("-fullscreen", True)

    add_background(home_root, r"background_image.png")

    start_button = tk.Button(home_root,height= 1, width=15, text="Start Game", font=("COPPERPLATE GOTHIC BOLD", 25), command=start_game)
    start_button.place(relx=0.5, rely=0.6, anchor=tk.CENTER)

    instructions_button = tk.Button(home_root,height =1, width = 15,text="Instructions", font=("COPPERPLATE GOTHIC BOLD", 25), command=show_instructions)
    instructions_button.place(relx=0.5, rely=0.7, anchor=tk.CENTER)

    exit_button = tk.Button(home_root,width = 10, text="Exit", font=("COPPERPLATE GOTHIC BOLD", 25), command=lambda: exit_application(home_root))
    exit_button.place(relx=0.5, rely=0.8, anchor=tk.CENTER)

    home_root.mainloop()

def start_game():
    
    home_root.destroy()  
    level_selection_window()

home_window()
