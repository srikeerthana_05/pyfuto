import random
import pygame
import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk

# Function to print the grid for debugging
def print_grid(grid):
    """Prints the grid in a formatted way."""
    for row in grid:
        print(" ".join(str(val) if val != 0 else "." for val in row))
    print()

# Check if a number is valid in the given position
def is_valid(grid, row, col, num, inequalities):
    """Checks if a number can be placed at a given position respecting the rules."""
    # Check if the number is already in the row or column
    for i in range(4):
        if grid[row][i] == num or grid[i][col] == num:
            return False

    # Check inequalities
    for (r1, c1, r2, c2, relation) in inequalities:
        if r1 == row and c1 == col:  # Position is a "left" side of an inequality
            if relation == '<' and not (grid[r2][c2] == 0 or num < grid[r2][c2]):
                return False
            if relation == '>' and not (grid[r2][c2] == 0 or num > grid[r2][c2]):
                return False
        elif r2 == row and c2 == col:  # Position is a "right" side of an inequality
            if relation == '<' and not (grid[r1][c1] == 0 or grid[r1][c1] < num):
                return False
            if relation == '>' and not (grid[r1][c1] == 0 or grid[r1][c1] > num):
                return False

    return True

# Solver function using backtracking
def solve_futoshiki(grid, inequalities):
    """Attempts to solve the Futoshiki puzzle using backtracking."""
    for row in range(4):
        for col in range(4):
            if grid[row][col] == 0:  # Find an empty cell
                for num in range(1, 5):  # Numbers 1 to 4 for a 4x4 grid
                    if is_valid(grid, row, col, num, inequalities):
                        grid[row][col] = num
                        if solve_futoshiki(grid, inequalities):
                            return True
                        grid[row][col] = 0  # Backtrack

                return False  # No valid number found, trigger backtracking
    return True

# Generate a complete grid
def generate_filled_grid():
    """Generates a completely filled 4x4 Futoshiki grid."""
    grid = [[0 for _ in range(4)] for _ in range(4)]
    nums = list(range(1, 5))

    # Fill the grid row by row
    for row in range(4):
        # Attempt to find a valid permutation of the row
        attempts = 0
        while True:
            attempts += 1
            random.shuffle(nums)
            grid[row] = nums[:]
            if solve_futoshiki(grid, []):  # Ensure the partial grid is valid
                break
            if attempts > 10:  # If stuck, restart
                return generate_filled_grid()

    return grid

# Create a puzzle by removing some numbers
def create_puzzle(grid, num_holes=6):
    """Creates a Futoshiki puzzle by removing numbers from the filled grid."""
    puzzle = [row[:] for row in grid]  # Make a copy of the grid
    removed_positions = set()

    while len(removed_positions) < num_holes:
        row = random.randint(0, 3)
        col = random.randint(0, 3)
        if (row, col) not in removed_positions:
            puzzle[row][col] = 0
            removed_positions.add((row, col))

    return puzzle

# Generate random inequalities based on the filled grid
def generate_inequalities(grid):
    """Generates random inequalities based on the filled grid."""
    inequalities = []
    # Use sets to track which inequalities have been used to avoid duplicates
    inequality_positions = set()

    while len(inequalities) < 5:  # Attempt to generate 5 inequalities
        row1, col1 = random.randint(0, 3), random.randint(0, 3)
        direction = random.choice(['row', 'col'])
        if direction == 'row' and col1 < 3:  # Ensure it stays within the grid
            row2, col2 = row1, col1 + 1
        elif direction == 'col' and row1 < 3:
            row2, col2 = row1 + 1, col1
        else:
            continue

        # Ensure uniqueness of inequality constraints
        if (row1, col1, row2, col2) in inequality_positions or (row2, col2, row1, col1) in inequality_positions:
            continue

        # Append inequality if valid and store position
        if grid[row1][col1] < grid[row2][col2]:
            inequalities.append((row1, col1, row2, col2, '<'))
            inequality_positions.add((row1, col1, row2, col2))
        elif grid[row1][col1] > grid[row2][col2]:
            inequalities.append((row1, col1, row2, col2, '>'))
            inequality_positions.add((row1, col1, row2, col2))

    return inequalities

# Generate puzzle and ensure it is solvable
def generate_puzzle_and_solution():
    """Generates a puzzle and its solution ensuring it is solvable."""
    while True:
        filled_grid = generate_filled_grid()
        inequalities = generate_inequalities(filled_grid)
        puzzle = create_puzzle(filled_grid)

        solution = [row[:] for row in puzzle]  # Copy the puzzle to solve
        if solve_futoshiki(solution, inequalities):
            return puzzle, solution, inequalities

# Draw the Futoshiki puzzle using Pygame
def draw_puzzle(screen, grid, inequalities, cell_size=100, gap_size=38, show_solution=False, player_grid=None):
    """Draws the Futoshiki puzzle using Pygame."""
    font = pygame.font.Font(None, 72)
    small_font = pygame.font.Font(None, 48)  # Increase font size for inequalities
    screen.fill((220, 220, 220))  # Light gray background for better contrast

    for row in range(4):
        for col in range(4):
            x = col * (cell_size + gap_size)
            y = row * (cell_size + gap_size)

            # Draw the cell rectangle
            rect = pygame.Rect(x, y, cell_size, cell_size)
            pygame.draw.rect(screen, (255, 255, 255), rect)
            pygame.draw.rect(screen, (0, 0, 0), rect, 2)  # Thicker border for clarity

            # Draw the number
            if player_grid is not None and player_grid[row][col] != 0:
                num = player_grid[row][col]
                text = font.render(str(num), True, (0, 128, 0))  # Player input in green
            else:
                num = grid[row][col] if not show_solution else solution[row][col]
                if num != 0:
                    text = font.render(str(num), True, (0, 0, 0))
            if num != 0:
                text_rect = text.get_rect(center=(x + cell_size / 2, y + cell_size / 2))
                screen.blit(text, text_rect)

    # Draw inequalities
    for (r1, c1, r2, c2, relation) in inequalities:
        if r1 == r2:  # Horizontal inequality
            x = min(c1, c2) * (cell_size + gap_size) + cell_size + (gap_size // 2)
            y = r1 * (cell_size + gap_size) + cell_size / 2
            if relation == '>':
                arrow = small_font.render('>', True, (0, 0, 0))
            else:
                arrow = small_font.render('<', True, (0, 0, 0))
            screen.blit(arrow, (x - arrow.get_width() / 2, y - arrow.get_height() / 2))
        elif c1 == c2:  # Vertical inequality
            x = c1 * (cell_size + gap_size) + cell_size / 2
            y = min(r1, r2) * (cell_size + gap_size) + cell_size + (gap_size // 2)
            if relation == '>':
                arrow = small_font.render('v', True, (0, 0, 0))  # Downward arrow
            else:
                arrow = small_font.render('^', True, (0, 0, 0))  # Upward arrow
            screen.blit(arrow, (x - arrow.get_width() / 2, y - arrow.get_height() / 2))

# Tkinter button callback to show the solution
def show_solution():
    """Callback function to show the solution in the Pygame window."""
    global solution_displayed
    solution_displayed = True
    messagebox.showinfo("Solution", "The solution is now displayed.")

# Tkinter button callback to submit the player's solution
def submit_solution():
    """Callback function to submit the player's solution."""
    if player_grid == solution:
        messagebox.showinfo("Result", "Congratulations! You solved the puzzle!")
    else:
        messagebox.showinfo("Result", "Sorry, your solution is incorrect. The correct solution is now displayed.")
    show_solution()

# Initialize Pygame and Tkinter
pygame.init()
root = tk.Tk()
root.title("Futoshiki Puzzle")

# Generate puzzle
puzzle, solution, inequalities = generate_puzzle_and_solution()
player_grid = [row[:] for row in puzzle]  # Create a grid for player input
print("Generated Futoshiki Puzzle:")
print_grid(puzzle)
print("Solution:")
print_grid(solution)
print("Inequalities:")
for ineq in inequalities:
    print(f"Cell ({ineq[0] + 1},{ineq[1] + 1}) {ineq[4]} Cell ({ineq[2] + 1},{ineq[3] + 1})")

# Pygame window inside a Tkinter window
cell_size = 100  # Size of each cell
gap_size = 38    # Gap between cells to accommodate inequalities
screen = pygame.Surface((4 * (cell_size + gap_size) - gap_size, 4 * (cell_size + gap_size) - gap_size))

# Draw the initial puzzle
solution_displayed = False
draw_puzzle(screen, puzzle, inequalities, cell_size=cell_size, gap_size=gap_size)

# Display Pygame surface in Tkinter window
canvas = tk.Canvas(root, width=4 * (cell_size + gap_size) - gap_size, height=4 * (cell_size + gap_size) - gap_size)
canvas.pack()

# Convert Pygame surface to a format suitable for Tkinter
def update_display():
    """Updates the display with the current state of the Pygame surface."""
    global solution_displayed
    draw_puzzle(screen, puzzle, inequalities, cell_size=cell_size, gap_size=gap_size, show_solution=solution_displayed, player_grid=player_grid)
    
    # Convert Pygame surface to Pillow image
    image_string = pygame.image.tostring(screen, "RGB")
    image_surface = Image.frombytes("RGB", screen.get_size(), image_string)
    photo = ImageTk.PhotoImage(image_surface)

    # Update canvas
    canvas.create_image(0, 0, anchor=tk.NW, image=photo)
    canvas.image = photo  # Keep a reference to prevent garbage collection

    root.after(100, update_display)

# Add a button to show the solution
solution_button = tk.Button(root, text="Show Solution", command=show_solution)
solution_button.pack()

# Add a button to submit the player's solution
submit_button = tk.Button(root, text="Submit Solution", command=submit_solution)
submit_button.pack()

# Handle keyboard events to fill in the grid
def handle_key_event(event):
    """Handles keyboard input for filling the grid."""
    if event.keysym.isdigit() and 1 <= int(event.keysym) <= 4:
        num = int(event.keysym)
        row, col = selected_cell
        if puzzle[row][col] == 0:  # Allow input only in empty cells
            player_grid[row][col] = num

def handle_mouse_click(event):
    """Handles mouse clicks to select cells."""
    x, y = event.x, event.y
    col = x // (cell_size + gap_size)
    row = y // (cell_size + gap_size)
    if 0 <= row < 4 and 0 <= col < 4:
        global selected_cell
        selected_cell = (row, col)

selected_cell = (0, 0)  # Initial selected cell

# Bind events
canvas.bind("<Button-1>", handle_mouse_click)
root.bind("<Key>", handle_key_event)

# Start updating display and Tkinter main loop
update_display()
root.mainloop()
pygame.quit()
