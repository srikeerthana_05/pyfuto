import pygame
import sys

pygame.init()

info = pygame.display.Info()
screen_width, screen_height = info.current_w, info.current_h

screen = pygame.display.set_mode((screen_width, screen_height), pygame.FULLSCREEN)
pygame.display.set_caption("Game Window")

pygame.display.set_caption("Fullscreen Pygame Window")

background_image = pygame.image.load('background_image.png')
background_image = pygame.transform.scale(background_image, (screen_width, screen_height))

mainmenu_image = pygame.image.load('mainmenu.png')
mainmenu_image = pygame.transform.scale(mainmenu_image, (screen_width, screen_height))

instructions_image = pygame.image.load('instructions.png')
instructions_image = pygame.transform.scale(instructions_image, (screen_width, screen_height))

button_font = pygame.font.Font(None, 60)
input_font = pygame.font.Font(None, 60)

def create_button(text, font, text_color, box_color, center_position, width, height):
    button_surface = pygame.Surface((width, height))
    button_surface.fill(box_color)
    button_rect = button_surface.get_rect(center=center_position)
    text_surface = font.render(text, True, text_color)
    text_rect = text_surface.get_rect(center=button_rect.center)
    return button_surface, button_rect, text_surface, text_rect

def create_checkbox(center_position, size = 40):
    box_surface = pygame.Surface((size, size))
    box_rect = box_surface.get_rect(center=center_position)
    box_surface.fill((255, 255, 255))
    pygame.draw.rect(box_surface, (0, 0, 0), box_rect, 2)
    return box_surface, box_rect

button_width, button_height = 300, 100
button_color = (0, 0, 0)
text_color = (245, 245, 220)

button_spacing = 150

start_button, start_button_rect, start_text, start_text_rect = create_button("Start", button_font, text_color, button_color, (screen_width // 2, screen_height // 2 + button_spacing), button_width, button_height)
instructions_button, instructions_button_rect, instructions_text, instructions_text_rect = create_button("Instructions", button_font, text_color, button_color, (screen_width // 2, screen_height // 2 + 2 * button_spacing), button_width, button_height)
quit_button, quit_button_rect, quit_text, quit_text_rect = create_button("Quit", button_font, text_color, button_color, (screen_width // 2, screen_height // 2 + 3 * button_spacing), button_width, button_height)

def main_menu():
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                    pygame.quit()
                    sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if start_button_rect.collidepoint(event.pos):
                    get_user_name()
                elif instructions_button_rect.collidepoint(event.pos):
                    display_instructions()
                elif quit_button_rect.collidepoint(event.pos):
                    running = False
                    pygame.quit()
                    sys.exit()

        screen.blit(background_image, (0, 0))

        screen.blit(start_button, start_button_rect)
        screen.blit(start_text, start_text_rect)
        screen.blit(instructions_button, instructions_button_rect)
        screen.blit(instructions_text, instructions_text_rect)
        screen.blit(quit_button, quit_button_rect)
        screen.blit(quit_text, quit_text_rect)

        pygame.display.flip()

def get_user_name():
    input_active = True
    user_text = ''
    file_selected = False
    play_selected = False

    file_checkbox, file_checkbox_rect = create_checkbox((screen_width // 2 - 100, screen_height // 2 + 50))
    play_checkbox, play_checkbox_rect = create_checkbox((screen_width // 2 - 100, screen_height // 2 + 150))

    while input_active:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                input_active = False
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    input_active = False
                    main_menu()
                elif event.key == pygame.K_RETURN:
                    input_active = False
                    welcome_user(user_text, file_selected, play_selected)
                elif event.key == pygame.K_BACKSPACE:
                    user_text = user_text[:-1]
                else:
                    user_text += event.unicode
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if file_checkbox_rect.collidepoint(event.pos):
                    file_selected = not file_selected
                    play_selected = False
                elif play_checkbox_rect.collidepoint(event.pos):
                    play_selected = not play_selected
                    file_selected = False

        screen.blit(mainmenu_image, (0, 0))

        prompt_surface = input_font.render("Enter your name:", True, (255, 255, 255))
        screen.blit(prompt_surface, (screen_width // 2 - prompt_surface.get_width() // 2, screen_height // 2 - 150))

        input_surface = input_font.render(user_text, True, (255, 255, 255))
        screen.blit(input_surface, (screen_width // 2 - input_surface.get_width() // 2, screen_height // 2 - 50))

        screen.blit(file_checkbox, file_checkbox_rect)
        screen.blit(play_checkbox, play_checkbox_rect)
        file_label_surface = input_font.render("Insert File", True, (255, 255, 255))
        play_label_surface = input_font.render("Play Game", True, (255, 255, 255))
        screen.blit(file_label_surface, (file_checkbox_rect.x + 50, file_checkbox_rect.y))
        screen.blit(play_label_surface, (play_checkbox_rect.x + 50, play_checkbox_rect.y))

        if file_selected:
            pygame.draw.line(screen, (0, 0, 0), (file_checkbox_rect.left + 10, file_checkbox_rect.centery), (file_checkbox_rect.centerx, file_checkbox_rect.bottom - 10), 5)
            pygame.draw.line(screen, (0, 0, 0), (file_checkbox_rect.centerx, file_checkbox_rect.bottom - 10), (file_checkbox_rect.right - 10, file_checkbox_rect.top + 10), 5)
        if play_selected:
            pygame.draw.line(screen, (0, 0, 0), (play_checkbox_rect.left + 10, play_checkbox_rect.centery), (play_checkbox_rect.centerx, play_checkbox_rect.bottom - 10), 5)
            pygame.draw.line(screen, (0, 0, 0), (play_checkbox_rect.centerx, play_checkbox_rect.bottom - 10), (play_checkbox_rect.right - 10, play_checkbox_rect.top + 10), 5)

        pygame.display.flip()

def welcome_user(user_name, file_selected, play_selected):
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                    main_menu()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pass

        screen.fill((0, 0, 0))

        welcome_surface = input_font.render("Welcome, " + user_name, True, (255, 255, 255))
        screen.blit(welcome_surface, (screen_width // 2 - welcome_surface.get_width() // 2, screen_height // 2 - button_spacing))

        if file_selected:
            action_surface = input_font.render("You selected: Insert File", True, (255, 255, 255))
        elif play_selected:
            
            
            action_surface = input_font.render("You selected: Play Game", True, (255, 255, 255))
        else:
            action_surface = input_font.render("No selection made", True, (255, 255, 255))
        
        screen.blit(action_surface, (screen_width // 2 - action_surface.get_width() // 2, screen_height // 2 + button_spacing))
        pygame.display.flip()

def display_instructions():
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                    main_menu()

    
        screen.blit(instructions_image, (0, 0))

        pygame.display.flip()
main_menu()
