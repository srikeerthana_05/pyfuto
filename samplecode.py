import cv2
import pytesseract

def preprocess_image(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    _, thresh = cv2.threshold(image, 150, 255, cv2.THRESH_BINARY_INV)
    return thresh

image_path = "futo1.png" 
processed_image = preprocess_image(image_path)
cv2.imshow('Processed Image', processed_image)
cv2.waitKey(0)
cv2.destroyAllWindows()

pygame.init()
screen = pygame.display.set_mode((600, 600))
pygame.display.set_caption('Futoshiki Game')

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill((255, 255, 255))
    display_puzzle(screen, grid, constraints)
    pygame.display.flip()

pygame.quit()
