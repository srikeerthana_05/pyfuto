import tkinter as tk
from tkinter import messagebox
from futoshiki_logic import generate_puzzle_and_solution, solve_futoshiki

def clear_frame(frame):
    if frame is not None:
        frame.destroy()

def show_start_page(root):
    clear_frame(root.frame)
    root.frame = tk.Frame(root)
    root.frame.pack(fill="both", expand=True)

    start_button = tk.Button(root.frame, text="Start Game", command=lambda: show_level_selection_page(root))
    start_button.pack(pady=20)

    exit_button = tk.Button(root.frame, text="Exit", command=root.quit)
    exit_button.pack(pady=20)

def show_level_selection_page(root):
    clear_frame(root.frame)
    root.frame = tk.Frame(root)
    root.frame.pack(fill="both", expand=True)

    levels = ["Easy", "Medium", "Hard", "Expert"]
    for level in levels:
        level_button = tk.Button(root.frame, text=level, command=lambda l=level: start_game(root, l))
        level_button.pack(pady=10)

    back_button = tk.Button(root.frame, text="Back to Start", command=lambda: show_start_page(root))
    back_button.pack(pady=20)

def start_game(root, level):
    root.level = level
    difficulty = {
        "Easy": 0.3,
        "Medium": 0.5,
        "Hard": 0.7,
        "Expert": 0.8
    }

    if level == "Expert":
        root.grid_size = root.current_level
    else:
        root.grid_size = {
            "Easy": 4,
            "Medium": 5,
            "Hard": 6
        }[level]

    root.puzzle, root.solution, root.inequalities = generate_puzzle_and_solution(root.grid_size, difficulty[level])
    show_puzzle_page(root)

def show_puzzle_page(root):
    clear_frame(root.frame)
    root.frame = tk.Frame(root)
    root.frame.pack(fill="both", expand=True)

    root.cells = []
    for r in range(root.grid_size):
        row = []
        for c in range(root.grid_size):
            entry = tk.Entry(root.frame, width=2, font=("Arial", 18), justify="center")
            entry.grid(row=r, column=c, padx=5, pady=5)
            if root.puzzle[r][c] != 0:
                entry.insert(0, str(root.puzzle[r][c]))
                entry.config(state="disabled")
            row.append(entry)
        root.cells.append(row)

    show_solution_button = tk.Button(root.frame, text="Show Solution", command=lambda: show_solution(root))
    show_solution_button.grid(row=root.grid_size, column=0, columnspan=root.grid_size//2, pady=10)

    submit_button = tk.Button(root.frame, text="Submit", command=lambda: check_solution(root))
    submit_button.grid(row=root.grid_size, column=root.grid_size//2, columnspan=root.grid_size//2, pady=10)

    return_button = tk.Button(root.frame, text="Return to Start", command=lambda: show_start_page(root))
    return_button.grid(row=root.grid_size+1, column=0, columnspan=root.grid_size//2, pady=10)

    if root.grid_size < 9:
        next_puzzle_button = tk.Button(root.frame, text="Next Puzzle", command=lambda: start_game_with_current_level(root))
    else:
        next_puzzle_button = tk.Button(root.frame, text="Restart Expert Level", command=lambda: restart_expert_level(root))
        
    next_puzzle_button.grid(row=root.grid_size+1, column=root.grid_size//2, columnspan=root.grid_size//2, pady=10)

def restart_expert_level(root):
    root.current_level = 4
    start_game(root, "Expert")

def show_solution(root):
    for r in range(root.grid_size):
        for c in range(root.grid_size):
            root.cells[r][c].delete(0, tk.END)
            root.cells[r][c].insert(0, str(root.solution[r][c]))

def check_solution(root):
    user_solution = []
    for r in range(root.grid_size):
        row = []
        for c in range(root.grid_size):
            try:
                value = int(root.cells[r][c].get())
            except ValueError:
                messagebox.showerror("Error", "Please fill all cells with numbers!")
                return
            row.append(value)
        user_solution.append(row)

    if user_solution == root.solution:
        messagebox.showinfo("Congratulations", "You solved the puzzle!")
    else:
        messagebox.showerror("Failed", "Incorrect solution, please try again!")

def start_game_with_current_level(root):
    if root.level == "Expert" and root.current_level < 9:
        root.current_level += 1
    start_game(root, root.level)


# Initialize and run the application
root = tk.Tk()
root.title("Futoshiki Game")
root.geometry("600x600")
root.frame = None
root.grid_size = None
root.puzzle = None
root.solution = None
root.inequalities = None
root.current_level = 4  # Used for Expert level tracking
root.level = None  # Track the current level

# Start the game by showing the start page
show_start_page(root)

# Run the application
root.mainloop()

