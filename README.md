PyFuto

Authors:

Keerthana Sadanala

Anvitha Rao Pinninti

Project Overview:

PyFuto is a Futoshiki puzzle solver that generates random puzzles and provides solutions. It utilizes Python along with Tkinter and Pygame to create an interactive and user-friendly interface.