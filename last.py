import cv2
import pytesseract
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# Configure tesseract executable path if needed
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def preprocess_image(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    _, thresh = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY_INV)
    return thresh

def extract_text(image):
    custom_config = r'--oem 3 --psm 6'
    text = pytesseract.image_to_string(image, config=custom_config)
    return text

def parse_puzzle(text):
    lines = text.strip().split('\n')
    puzzle = np.zeros((5, 5), dtype=int)
    inequalities = []

    for i, line in enumerate(lines):
        for j, char in enumerate(line.strip()):
            if char.isdigit():
                puzzle[i, j] = int(char)
            elif char in {'<', '>', '^', 'v'}:
                inequalities.append((i, j, char))

    return puzzle, inequalities

def display_puzzle(puzzle, inequalities):
    fig, ax = plt.subplots()
    ax.set_xlim(0, 5)
    ax.set_ylim(0, 5)
    ax.set_xticks(np.arange(6))
    ax.set_yticks(np.arange(6))
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.grid(which='both')

    for i in range(5):
        for j in range(5):
            if puzzle[i, j] != 0:
                ax.text(j + 0.5, 4.5 - i, str(puzzle[i, j]),
                        ha='center', va='center', fontsize=20)

    for ineq in inequalities:
        x, y, direction = ineq
        if direction == 'v':
            ax.add_patch(patches.Arrow(y + 0.5, 4.5 - x, 0, -0.5, width=0.2, color='black'))
        elif direction == 'r':
            ax.add_patch(patches.Arrow(y + 0.5, 4.5 - x, 0.5, 0, width=0.2, color='black'))
        elif direction == 'l':
            ax.add_patch(patches.Arrow(y + 0.5, 4.5 - x, -0.5, 0, width=0.2, color='black'))

    plt.gca().set_aspect('equal', adjustable='box')
    plt.savefig('solved_puzzle.png')  # Save the figure instead of showing it
    plt.close(fig)

def solve_futoshiki(puzzle, inequalities):
    def is_valid(puzzle, row, col, num):
        for i in range(5):
            if puzzle[row, i] == num or puzzle[i, col] == num:
                return False
        return True

    def apply_inequalities(puzzle, inequalities):
        for x, y, direction in inequalities:
            if direction == 'v' and x < 4:
                if puzzle[x, y] != 0 and puzzle[x+1, y] != 0 and puzzle[x, y] >= puzzle[x+1, y]:
                    return False
            elif direction == 'r' and y < 4:
                if puzzle[x, y] != 0 and puzzle[x, y+1] != 0 and puzzle[x, y] >= puzzle[x, y+1]:
                    return False
            elif direction == 'l' and y > 0:
                if puzzle[x, y] != 0 and puzzle[x, y-1] != 0 and puzzle[x, y] <= puzzle[x, y-1]:
                    return False
        return True

    def solve(puzzle, inequalities):
        for i in range(5):
            for j in range(5):
                if puzzle[i, j] == 0:
                    for num in range(1, 6):
                        if is_valid(puzzle, i, j, num):
                            puzzle[i, j] = num
                            if apply_inequalities(puzzle, inequalities) and solve(puzzle, inequalities):
                                return True
                            puzzle[i, j] = 0
                    return False
        return True

    solved_puzzle = puzzle.copy()
    if solve(solved_puzzle, inequalities):
        return solved_puzzle
    else:
        return None

def main(image_path):
    image = preprocess_image(image_path)
    text = extract_text(image)
    puzzle, inequalities = parse_puzzle(text)

    solved_puzzle = solve_futoshiki(puzzle, inequalities)
    if solved_puzzle is not None:
        display_puzzle(solved_puzzle, inequalities)
        print("Solved puzzle saved as 'solved_puzzle.png'.")
    else:
        print("No solution found.")

# Replace 'path_to_image.png' with the path to your image file
main('input_puzzle.png')

